﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jenkins.Api
{
    public class JenkinsMasterView
    {
        public string mode { get; set; }
        public string nodeDescription { get; set; }
        public Collection<JenkinsProject> jobs { get; set; }
    }
}
