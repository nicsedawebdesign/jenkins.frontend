﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jenkins.Frontend.Web.Models
{
    public class JenkinsMasterModel
    {
        public List<JenkinsJobModel> Jobs { get; set; }
    }
}