﻿using Jenkins.Api;
using Jenkins.Frontend.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Jenkins.Frontend.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            JenkinsMasterModel model = new JenkinsMasterModel();

            using (Jenkins.Api.Jenkins jenkins = new Jenkins.Api.Jenkins("http://localhost:8099"))
            {
                jenkins.GetJenkinsVersion();
                JenkinsMasterView view = jenkins.GetJenkinsInformation();

                if (view.jobs.Any())
                {
                    model.Jobs = new List<JenkinsJobModel>();
                    foreach (var j in view.jobs)
                    {
                        JenkinsJobModel job = new JenkinsJobModel();
                        job.Name = j.name;
                        job.Colour = j.color;
                        model.Jobs.Add(job);
                    }
                }
            }

            return View(model);
        }

        public ActionResult Build(string name)
        {
            using (Jenkins.Api.Jenkins jenkins = new Jenkins.Api.Jenkins("http://localhost:8099"))
            {
                int? queueId = jenkins.Build(name, new Dictionary<string, string>());
            }

            var client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:8099");





            return RedirectToAction("Index");
        }
    }
}