﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jenkins.Frontend.Web.Models
{
    public class JenkinsJobModel
    {

        public string Name { get; set; }

        public string Colour { get; set; }
    }
}
