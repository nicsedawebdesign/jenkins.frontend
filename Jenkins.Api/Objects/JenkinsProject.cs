﻿using System.Collections.ObjectModel;

namespace Jenkins.Api
{
    public class JenkinsProject
    {
        public string description { get; set; }
        public string displayName { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public bool buildable { get; set; }
        public Collection<JenkinsBuildItem> builds { get; set; }
        public string color { get; set; }
        public JenkinsBuildItem firstBuild { get; set; }
        public Collection<JenkinsHealthReport> healthReport { get; set; }
        public bool inQueue { get; set; }
        public bool keepDependencies { get; set; }
        public JenkinsBuildItem lastBuild { get; set; }
        public JenkinsBuildItem lastCompletedBuild { get; set; }
        public JenkinsBuildItem lastFailedBuild { get; set; }
        public JenkinsBuildItem lastStableBuild { get; set; }
        public JenkinsBuildItem lastUnsuccessfulBuild { get; set; }
        public int nextBuildNumber { get; set; }
        public Collection<JenkinsPropertyItem> property { get; set; }
        public bool concurrentBuild { get; set; }
    }
}
