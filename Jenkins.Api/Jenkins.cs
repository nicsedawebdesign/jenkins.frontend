﻿using Jenkins.Api.Exceptions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Linq;

namespace Jenkins.Api
{
    public class Jenkins : IDisposable
    {
        private HttpClient client = null;

        public string Uri { get; set; }

        internal string Username { get; set; }

        internal string Password { get; set; }

        public int Timeout { get; set; }

        public Jenkins(string uri)
            : this(uri, null, null, 40000)
        {
        }

        public Jenkins(string uri, int timeout)
            : this(uri, null, null, 40000)
        {
        }

        public Jenkins(string uri, string username, string password)
            : this(uri, username, password, 40000)
        {
        }

        public Jenkins(string uri, string username, string password, int timeout)
        {
            this.Uri = uri;
            this.Username = username;
            this.Password = password;
            this.Timeout = timeout;

            this.CreateConnection();
        }

        private void CreateConnection()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(this.Uri);

            if (!string.IsNullOrWhiteSpace(this.Username))
            {
                var buffer = Encoding.ASCII.GetBytes(this.Username + ":" + this.Password);
                var authHeader = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(buffer));
                client.DefaultRequestHeaders.Authorization = authHeader;
            }
        }

        public string GetJenkinsVersion()
        {
            try
            {
                var response = client.GetAsync("/api/json").Result;

                if (response.IsSuccessStatusCode)
                {
                    if (response.Headers.Contains("X-Jenkins"))
                    {
                        var items = response.Headers.GetValues("X-Jenkins");

                        return items.FirstOrDefault();
                    }
                }

                throw new JenkinsException("Could not return jenkins version");
            }
            catch (AggregateException ex)
            {
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    throw new JenkinsException("Could not connect to Jenkins");
                }
            }

            return null;
        }

        public JenkinsMasterView GetJenkinsInformation()
        {
            try
            {
                var response = client.GetAsync("/api/json").Result;

                if (response.IsSuccessStatusCode)
                {
                    string responseBody = response.Content.ReadAsStringAsync().Result;
                    JenkinsMasterView view = JsonConvert.DeserializeObject<JenkinsMasterView>(responseBody);
                    return view;
                }

                throw new JenkinsException("Could not return jenkins information");
            }
            catch (AggregateException ex)
            {
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    throw new JenkinsException("Could not connect to Jenkins");
                }
            }

            return null;
        }

        public int? Build(string name, Dictionary<string, string> parameters)
        {
            throw new NotImplementedException();
            try
            {
                var response = client.PostAsync(string.Format("/job/{0}/build", name), null).Result;

                if (response.IsSuccessStatusCode)
                {
                    string responseBody = response.Content.ReadAsStringAsync().Result;
                    //JenkinsMasterView view = JsonConvert.DeserializeObject<JenkinsMasterView>(responseBody);
                    //return view;
                }

                throw new JenkinsException("Could not return jenkins information");
            }
            catch (AggregateException ex)
            {
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    throw new JenkinsException("Could not connect to Jenkins");
                }
            }

            return null;
        }

        public void Dispose()
        {
            client.Dispose();
            client = null;
        }
    }
}
