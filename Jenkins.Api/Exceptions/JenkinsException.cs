﻿using System;

namespace Jenkins.Api.Exceptions
{
    [Serializable]
    public class JenkinsException : Exception
    {
        public JenkinsException()
        {
        }

        public JenkinsException(string message)
            : base(message)
        {
        }

        public JenkinsException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected JenkinsException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
    }
}
